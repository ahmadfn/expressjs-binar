const {landingPage, usersShow, userDetail, userAddForm, userUpdateForm, userAdd, userUpdate, userDelete} = require('../controller/users.controller.js');
const upload = require('../middleware/uploadImage.js');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');


module.exports = (app) => {
	app.use(bodyParser.urlencoded({extended: true}));
	app.use(bodyParser.json());
	app.use(methodOverride('_method'));

	app.get('/', landingPage)
	app.get('/users', usersShow);
	app.get('/users/add-form', userAddForm);
	app.get('/users/:id', userDetail);
	app.get('/users/:id/update-form', userUpdateForm)

	app.post('/users/add', upload.single('avatar'), userAdd);

	app.delete('/users/:id', userDelete);
	app.put('/users/:id', upload.single('avatar'), userUpdate);
}