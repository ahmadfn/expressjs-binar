let users = require('../../users.json');
const uuid = require('uuid');
const fs = require('fs');


const landingPage = (req, res) => {
	res.render('index');
}

const usersShow = (req, res) => {
	res.render('usersList', {users: users});
}

const userDetail = (req, res) => {
	let filtered = users.filter(user => user.id.indexOf(req.params.id) !== -1);

	if (filtered.length !== 0) {
		res.render('userDetail', {
			userId: filtered[0].id,
			userImage: filtered[0].image,
			userName: filtered[0].name,
			userEmail: filtered[0].email,
			userTelp: filtered[0].telp
		});
	}
	else res.send({success: false, message: "user is not found"});
}

const userAddForm = (req, res) => {
	res.render('addUserForm');
}

const userAdd = (req, res) => {
	req.headers['content-type'] = 'application/json';

	let posted = {
		id: uuid(),
		name: req.body.name,
		email: req.body.email || '',
		telp: req.body.telp || '',
		image: req.file? `/static/images/${req.file.filename}` : 'https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png'
	}

	users = [...users, posted];

	fs.writeFile('users.json', JSON.stringify(users), err => {
		if (err) res.send(err);
		else res.redirect(`/users/${posted.id}`);
	});
}

const userDelete = (req, res) => {
	const userIndex = users.findIndex(user => user.id === req.params.id);

	if (userIndex !== -1) {
		users.splice(userIndex, 1);

		fs.writeFile('users.json', JSON.stringify(users), err => {
			if (err) res.send(err);
			else res.redirect('/users/');
			//else res.json({success: true, message: `user ${req.params.id} has been deleted`});
		});
	} else {
		res.json({
			success: false,
			message: 'user is not found'
		})
	}
}

const userUpdateForm = (req, res) => {
	const userIndex = users.findIndex(user => user.id === req.params.id);

	res.render('updateUserForm', {
		userId: req.params.id,
		userName: users[userIndex].name,
		userEmail: users[userIndex].email,
		userTelp: users[userIndex].telp,
		userImage: users[userIndex].image
	});
}

const userUpdate = (req, res) => {
	const userIndex = users.findIndex(user => user.id === req.params.id);

	req.headers['content-type'] = 'application/json';

	if (userIndex !== -1) {
		const updated = {
			id: req.params.id,
			name: req.body.name || users[userIndex].name,
			email: req.body.email || users[userIndex].email,
			telp: req.body.telp || users[userIndex].telp,
			image: req.file ? `/static/images/${req.file.filename}` : users[userIndex].image 
		}

		users[userIndex] = updated;

		fs.writeFile('users.json', JSON.stringify(users), err => {
			if (err) res.send(err);
			else res.redirect(`/users/${updated.id}`);
			//else res.json({success: true, message: `user ${req.params.id} has been updated`});
		});
	} else {
		res.render('404 User is not found');
		/*
		res.json({
			success: false,
			message: 'user is not found'
		});
		*/
	}
}

module.exports = {landingPage, usersShow, userDetail, userAddForm, userUpdateForm, userAdd, userUpdate, userDelete}