const express = require('express');
const app = express();
const path = require('path');


app.set('view engine', 'ejs');
app.use('/static', express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
  res.render('index');
});

require('./app/routes/users.routes.js')(app);

app.listen(8000, () => {
  console.log('The app listening on port 8000');
});
